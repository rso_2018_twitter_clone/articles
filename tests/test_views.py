from datetime import datetime
import json

from articles import app
from articles.models import PostAttachments, Posts
from articles.views import delete_attachments
from tests.test_case import TestCase


class HealthcheckTestCase(TestCase):
    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def test_ok(self):
        result = self.app.get('/health')

        self.assertEqual(result.status_code, 200)
        self.assertEqual(json.loads(result.data), {"alive": True})


class PostsTestCase(TestCase):
    maxDiff = None

    def test_get_list(self):
        date01 = datetime(2013, 11, 13, 0, 0)
        date23 = datetime(2015, 11, 13, 0, 0)
        dates = ['2013-11-13T00:00:00+00:00', '2013-11-13T00:00:00+00:00',
                 '2015-11-13T00:00:00+00:00', '2015-11-13T00:00:00+00:00']

        posts = [Posts(message='Example text', user_id=1, date=date01),
                 Posts(message='Example text1', user_id=2, date=date01),
                 Posts(message='Example text2', user_id=1, date=date23),
                 Posts(message='Example text3', user_id=2, date=date23)]

        attachments = [PostAttachments(posts[0], "first_posts_attachment"),
                       PostAttachments(posts[1], "second_posts_attachment"),
                       PostAttachments(posts[2], "third_posts_attachment"),
                       PostAttachments(posts[3], "fourth_posts_attachment")]

        Posts.save_all(*posts)
        result = self.app.get("/article")

        self.assertEqual(result.status_code, 200)
        self.assertCountEqual(result.get_json(), [
            {
                'Comments': [],
                'Deleted': p.Deleted,
                'Message': p.Message,
                'PostDate': d,
                'PostId': p.PostId,
                'UserId': p.UserId,
                'attachments': [{'Attachment': v.Attachment}],
            } for p, v, d in zip(posts, attachments, dates)
        ])

        result = self.app.get("/article?user_id=2&since=2014-04-03%2020-21-21")
        self.assertEqual(result.status_code, 200)
        self.assertCountEqual(result.get_json(), [
            {
                'PostId': posts[3].PostId,
                'UserId': 2,
                'Message': 'Example text3',
                'attachments': [{'Attachment': attachments[3].Attachment}],
                'Comments': [],
                'PostDate': '2015-11-13T00:00:00+00:00',
                'Deleted': False,
            }
        ])

    def test_get_with_comment(self):
        date01 = datetime(2013, 11, 13, 0, 0)
        date23 = datetime(2015, 11, 13, 0, 0)

        posts = [Posts(message='Example text', user_id=1, date=date01),
                 Posts(message='Example text1', user_id=2, date=date01),
                 Posts(message='Example text2', user_id=1, date=date23),
                 Posts(message='Example text3', user_id=2, date=date23)]

        comment = Posts(message='Comment message', user_id=2,
                        parent_id=posts[0].PostId, date=date23)

        posts.append(comment)
        Posts.save_all(*posts)
        result = self.app.get('/article/{0}'.format(posts[0].PostId))
        self.assertEqual(result.status_code, 200)
        self.assertCountEqual(result.get_json(), {
            'PostId': posts[0].PostId,
            'UserId': 1,
            'Message': 'Example text',
            'attachments': [],
            'Comments': [{
                'PostId': comment.PostId,
                'UserId': 2,
                'Message': 'Comment message',
                'attachments': [],
                'Comments': [],
                'PostDate': '2015-11-13T00:00:00+00:00',
                'Deleted': False,
            }],
            'PostDate': '2013-11-13T00:00:00+00:00',
            'Deleted': False,
        })

    def test_delete_attachments(self):
        date01 = datetime(2013, 11, 13, 0, 0)
        date23 = datetime(2015, 11, 13, 0, 0)

        posts = [Posts(message='Example text', user_id=1, date=date01),
                 Posts(message='Example text1', user_id=2, date=date01),
                 Posts(message='Example text2', user_id=1, date=date23),
                 Posts(message='Example text3', user_id=2, date=date23)]

        PostAttachments(posts[0], "first_posts_attachment")
        PostAttachments(posts[1], "second_posts_attachment")
        PostAttachments(posts[2], "third_posts_attachment")
        PostAttachments(posts[3], "fourth_posts_attachment")

        Posts.save_all(*posts)
        p = Posts.query.all()
        result = self.app.get("/article/1")
        self.assertEqual(result.get_json()['attachments'],
                         [{'Attachment': "first_posts_attachment"}],)

        for i in p:
            delete_attachments(i)

        result = self.app.get("/article/1")
        self.assertEqual(result.get_json()['attachments'], [])

    # def test_delete_post(self):
    #     date01 = datetime(2013, 11, 13, 0, 0)
    #     date23 = datetime(2015, 11, 13, 0, 0)
    #
    #     posts = [Posts(message='Example text', user_id=1, date=date01),
    #              Posts(message='Example text1', user_id=2, date=date01),
    #              Posts(message='Example text2', user_id=1, date=date23),
    #              Posts(message='Example text3', user_id=2, date=date23)]
    #
    #     Posts.save_all(*posts)
    #     for p in posts:
    #         # result = self.app.delete('/article/{0}'.format(p.PostId))
    #         result = views.delete_article(p.PostId)
    #         self.assertEqual(result.status_code, 200)
    #
    #     result = self.app.get('/article/')
    #     self.assertEqual(result.status_code, 404)
