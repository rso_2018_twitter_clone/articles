import unittest

from articles import app, db
from config import app_config


def get_app_client():
    app.config.from_object(app_config['testing'])
    # creates a test client
    client = app.test_client()
    # propagate the exceptions to the test client
    client.testing = True
    return client


class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = get_app_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
