#!/usr/bin/env python3
import unittest

from flask_migrate import MigrateCommand
from flask_script import Command, Manager

from articles import app


class TestCommand(Command):
    """ Runs tests using unittest. """
    def run(self):
        test_loader = unittest.defaultTestLoader
        test_runner = unittest.TextTestRunner()
        test_suite = test_loader.discover('.')
        test_runner.run(test_suite)


manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('test', TestCommand)

if __name__ == '__main__':
    manager.run()
