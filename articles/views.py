import logging

import dateutil.parser
from flask import abort, jsonify, request
from flask_jwt import current_identity, jwt_required
import requests
from sqlalchemy import desc

from articles import app, db
from articles.models import PostAttachments, Posts
from articles.schemas import post_schema, posts_schema


@app.route('/health')
def health():
    return jsonify({"alive": True})


def create_article(user_id, parent_id=None):
    content = request.get_json()

    if not {'content'}.issubset(content):
        abort(400)

    new_article = Posts(content['content'], user_id, parent_id)
    for v in content['attachments']:
        new_attachment = PostAttachments(new_article, v)
        db.session.add(new_attachment)

    db.session.add(new_article)
    db.session.commit()

    r = requests.post(
        '{0}/new_post'.format(app.config['HYDROGEN_URL']),
        json={'user_id': user_id, 'post_id': new_article.PostId},
    )
    if r.status_code != 200:
        logging.error("error sending new_post request %d", r.status_code)

    return jsonify({'message': "Article Created!"})


@app.route('/article', methods=['POST'])
@jwt_required()
def create_post():
    return create_article(int(current_identity))


@app.route('/article/<int:post_id>', methods=['POST'])
@jwt_required()
def create_comment(post_id):
    return create_article(int(current_identity), post_id)


@app.route('/article/<int:post_id>', methods=['GET'])
def get_article_by_id(post_id):
    article = Posts.query.get(post_id)
    if article is None:
        abort(404)

    return post_schema.jsonify(article)


@app.route('/article', methods=['GET'])
def get_articles():
    users_id = request.args.getlist('user_id')
    since = request.args.get('since')
    to = request.args.get('to')

    articles = Posts.query.order_by(desc(Posts.PostDate))\
        .filter(Posts.ParentId == None, Posts.Deleted == False)  # noqa

    if users_id:
        articles = articles.filter(Posts.UserId.in_(users_id))

    if since:
        articles = articles.filter(Posts.PostDate > dateutil.parser.parse(since))
    if to:
        articles = articles.filter(Posts.PostDate < dateutil.parser.parse(to))

    if articles is None:
        abort(404)

    return posts_schema.jsonify(articles.all())


def delete_attachments(article):
    attachments = PostAttachments.query.join(Posts).\
        filter(PostAttachments.PostId == article.PostId)

    for e in attachments:
        db.session.delete(e)

    db.session.commit()


@app.route('/article/<int:post_id>', methods=['PUT'])
@jwt_required()
def edit_article(post_id):
    content = request.get_json()
    article = Posts.query.get(post_id)

    if article is None:
        abort(404)

    if article.UserId != int(current_identity):
        return jsonify({'message': "Article can be updated by only by author"})

    delete_attachments(article)
    for v in content['attachments']:
        new_attachment = PostAttachments(article, v)
        db.session.add(new_attachment)

    article.Message = content['content']
    db.session.commit()
    return jsonify({'message': "Article Updated!"})


@app.route('/article/<int:post_id>', methods=['DELETE'])
@jwt_required()
def delete_article(post_id):
    article = Posts.query.get(post_id)

    if article is None:
        abort(404)

    if article.UserId != int(current_identity):
        return jsonify({'message': "Article can be deleted only by author"})

    delete_attachments(article)
    db.session.delete(article)
    db.session.commit()
    return jsonify({'message': "Article has been deleted!"})
