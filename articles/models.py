from datetime import datetime

from articles import db


class Posts(db.Model):
    __tablename__ = 'posts'
    PostId = db.Column(db.Integer, primary_key=True)
    UserId = db.Column(db.Integer)
    ParentId = db.Column(db.Integer, db.ForeignKey('posts.PostId'))
    Comments = db.relationship('Posts', cascade="all,delete")
    Message = db.Column(db.Text)
    PostDate = db.Column(db.DateTime)
    Deleted = db.Column(db.Boolean, nullable=False)

    def __init__(self, message, user_id, parent_id=None, date=None):
        self.Message = message
        self.ParentId = parent_id
        self.UserId = user_id
        self.PostDate = date
        self.Deleted = False

        if date is None:
            date = datetime.utcnow()

        self.PostDate = date

    def __str__(self):
        return '<Post %r>' % self.Message

    @staticmethod
    def save_all(*objs):
        db.session.begin_nested()
        db.session.add_all(objs)
        db.session.commit()


class PostAttachments(db.Model):
    __tablename__ = 'attachment'
    AttachmentId = db.Column(db.Integer, primary_key=True)
    PostId = db.Column(db.Integer, db.ForeignKey('posts.PostId'))
    Post = db.relationship('Posts', backref='attachments')
    # @TODO change to binary field
    Attachment = db.Column(db.Text)

    def __init__(self, post, attachment):
        self.Post = post
        self.Attachment = attachment
