from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate, upgrade
from flask_jwt import JWT
from flask_cors import CORS

from config import app_config


app = Flask(__name__)
app.config.from_object(app_config['development'])
CORS(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)

with app.app_context():
    upgrade()


def authenticate(username, password):
    return


def identity(payload):
    user_id = payload['identity']
    return user_id


jwt_a = JWT(app, authenticate, identity)

import articles.views  # NOQA
