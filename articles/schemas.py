from marshmallow import fields

from articles import ma
from articles.models import PostAttachments, Posts


class PostSchema(ma.ModelSchema):
    Comments = fields.Nested('self', many=True)
    attachments = fields.Nested('AttachmentSchema', only=['Attachment'], many=True)

    class Meta:
        model = Posts


class AttachmentSchema(ma.ModelSchema):
    class Meta:
        model = PostAttachments


post_schema = PostSchema()
posts_schema = PostSchema(many=True)
attachment_schema = AttachmentSchema()
attachments_schema = AttachmentSchema(many=True)
