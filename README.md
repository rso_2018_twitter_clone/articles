# articles - user-related API Service

## Project contents

### Code

* `config.py` - global configuration file
* `run.py` - Python script starting app in development mode
* `articles/` - main application package
* `articles/__init__.py` - initializes flask application
* `articles/views.py` - API endpoints definitions
* `articles/*.py` - modules used by app
* `tests/test_*.py` - test modules - one per app module

### Development tools

* `check_style.sh` - helper script used to run code quality tools (flake8, isort)
* `init_hooks.sh` - helper script creating necessary git hook symlink
* `requirements.txt` - pip requirements list
* `setup.cfg` - config used by various Python tools (flake8, isort)

## Setup

1. Create virtualenv with python3:

    ```
    $ mkvirtualenv -ppython3 -a `pwd` -r requirements.txt articles
    ```
    After that newly created virtualenv should be activated which is denoted by
    `(articles)` prefix in terminal prompt.

    ```
    (articles) user@hostame:~/workspace/articles$
    ```

2. Install hooks:

    ```
    (articles)$ ./init_hooks.sh
    ```

## Adding a dependency

To add new dependency simply install it with `pip` with virtualenv activated.

```sh
(articles)$ pip install somelib
```

Then you should regenerate `requrements.txt` by executing:

```
(articles)$ pip freeze > requirements.txt
```

## Tests

To run test suite execute the following:

```
(articles)$ python -m unittest
```

## Running

### Development

Simply run the `run.py` script with virtualenv activated:

```
(articles)$ ./run.py
# or
(articles)$ python run.py
```

### Docker
If you haven't used giltab registry before you need to log in:
```
$ docker login registry.gitlab.com
```

Do run latest version of articles container, simply run:
```
$ docker run registry.gitlab.com/rso_2018_twitter_clone/articles:latest
```

## CI
Continous Integration pipeline is divided into following stages:
1. Build
2. Test
3. Release

### Build
This stage pulls docker image from `python:alpine` image and installs python
requirements. Fresh container is pushed to gitlab registry with tag `articles:<refname>`.

### Test
In this stage image built in previous step is pulled and then test scripts are run
inside the container. This ensures that the code passes tests on target environment.
Note that the image entrypoint is `'python'` and shell is not installed so
test scripts are run as python modules.

### Release
This stage is run only on master. If an image passes tests, then it is tagged
as `articles:latest` and then pushed back into registry.
