#!/bin/bash

set -e

cmd="$@"

if [[ ! -z "${SQLALCHEMY_DATABASE_URI}" ]]; then
until psql ${SQLALCHEMY_DATABASE_URI} -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done
fi

>&2 echo "Postgres is up - executing command"
python $cmd
