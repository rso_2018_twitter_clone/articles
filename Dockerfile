FROM python:3.6

RUN apt-get update && \
    apt-get -y install postgresql-client curl

MAINTAINER Michał Błotniak "buoto336@gmail.com"

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000/tcp

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["manage.py", "runserver", "--host=0.0.0.0"]
